import path from 'path'
import fs from 'fs'

function routesFromJsonBlobs(dirPath, template) {
  return fs.readdirSync(dirPath).filter(fn => fn.endsWith('.json')).map(file => {
    let data = require(path.join(__dirname, dirPath,file))
    return {
      path: path.join(dirPath, file.replace('.json', '')),
      template: template,
      getData: () => ({
        data: data
      }),
    }
  })
}

export default {
  paths: {
    public: 'static',
    dist: 'public'
  },
  getRoutes: async () => {
    let layouts = routesFromJsonBlobs('./layouts/', 'src/containers/Layout')
    let switches = routesFromJsonBlobs('./switches/', 'src/containers/Switch')
    let vendors = routesFromJsonBlobs('./vendors/', 'src/containers/Vendor')

    return layouts.concat(switches).concat(vendors)
  },
  plugins: [
    [
      require.resolve('react-static-plugin-source-filesystem'),
      {
        location: path.resolve('./src/pages'),
      },
    ],
    require.resolve('react-static-plugin-sass'),
    require.resolve('react-static-plugin-svg'),
    require.resolve('react-static-plugin-reach-router'),
    require.resolve('react-static-plugin-sitemap'),
  ],
}

import React from 'react'
import { Root, Routes, addPrefetchExcludes } from 'react-static'
import { Head } from 'react-static'
import { Router } from 'components/Router'
import Dynamic from 'containers/Dynamic'

import './app.scss'
import 'semantic-ui-css/semantic.min.css'

// Any routes that start with 'dynamic' will be treated as non-static routes
addPrefetchExcludes(['dynamic'])

function App() {
  return (
    <Root>
      <Head>
        <meta charSet="UTF-8" />
        <title>Keyboards that Click - Custom Keyboards</title>
      </Head>
      <React.Suspense fallback={<em>Loading...</em>}>
        <Router>
          <Dynamic path="dynamic" />
          <Routes path="*" />
        </Router>
      </React.Suspense>
    </Root>
  )
}

export default App

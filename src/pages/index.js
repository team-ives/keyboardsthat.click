import React from 'react'
import Page from '../components/Page'

export default () => (
  <Page>
    <div style={{ textAlign: 'center' }}>
      <h1>Information for Keyboard Enthusiasts</h1>
    </div>
  </Page>
)

import React from 'react'
import Page from '../components/Page'
import Vendor from '../components/vendors/Vendor'
import importAll from '../lib/importAll'

const vendors = importAll(require.context('../../vendors/', true, /\.json$/))

let content = vendors.map((vendor) => {
  console.log(`${vendor.name}-${vendor.url}`)
  return <Vendor {...vendor} key={vendor.url}/>
})

export default () => (
  <Page>{ content }</Page>
)

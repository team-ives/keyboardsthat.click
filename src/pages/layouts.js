import React from 'react'
import { Head } from 'react-static'
import { Segment, Grid, Input, Label, Button } from 'semantic-ui-react'
import { Range } from 'rc-slider';
import Data from '../lib/data.js'
import ls from '../lib/localstorage.js'
import 'rc-slider/assets/index.css';
import Page from '../components/Page'

export default class Layouts extends React.Component {
  constructor(props){
    super(props);
    this.data = new Data()
    this.state = this.data.initialState()

    this.handleWidthChange = this.handleWidthChange.bind(this)
    this.handleHeightChange = this.handleHeightChange.bind(this)
    this.handleSplit = this.handleSplit.bind(this)
    this.handleStagger = this.handleStagger.bind(this)
  }

  handleWidthChange(value) {
    ls.set('currentWidth', value)
    this.setState({currentWidth: value});
    this.forceUpdate();
  }

  handleHeightChange(value) {
    ls.set('currentHeight', value)
    this.setState({currentHeight: value});
    this.forceUpdate();
  }

  handleSplit(event) {
    if(this.state.split === event.target.value) {
      this.setState({split: undefined})
      ls.rm('currentSplit')
    } else {
      this.setState({split: event.target.value})
      ls.set('currentSplit', event.target.value)
    }
    this.forceUpdate();
  }

  handleStagger(event) {
    if(this.state.stagger === event.target.value) {
      this.setState({stagger: undefined})
      ls.rm('currentStagger')
    } else {
      this.setState({stagger: event.target.value})
      ls.set('currentStagger', event.target.value)
    }
    this.forceUpdate();
  }

  render() {
    let renderedKeyboards = this.data.filteredKeyboards(this.state);
    return (
      <Page>
        <Head>
          <meta charSet="UTF-8" />
          <title>Layout Compendium Browser - Keyboards that Click</title>
        </Head>
        <Segment.Group>
          <Segment className='keyboard-filters'>
            <Label.Group size='large'>
              <Input>
                <Label>
                  {`Width: ${this.state.currentWidth[0]}u`}
                </Label>
                <Range className='rangeSlider' onChange={this.handleWidthChange} defaultValue={this.state.currentWidth || [this.state.minWidth, this.state.maxWidth]} min={this.state.minWidth} max={this.state.maxWidth}/>
                <Label>
                  {`${this.state.currentWidth[1]}u`}
                </Label>
              </Input>
              <Input>
                <Label>
                  {`Height: ${this.state.currentHeight[0]}u`}
                </Label>
                <Range className='rangeSlider' onChange={this.handleHeightChange} defaultValue={this.state.currentHeight || [this.state.minHeight, this.state.maxHeight]} min={this.state.minHeight} max={this.state.maxHeight}/>
                <Label>
                  {`${this.state.currentHeight[1]}u`}
                </Label>
              </Input>
              <Button.Group>
                <Button onClick={this.handleSplit} className={this.state.split === 'full' ? 'yellow' : ''} value='full'>Full Split</Button>
                <Button.Or />
                <Button onClick={this.handleSplit} className={this.state.split === 'partial' ? 'yellow' : ''} value='partial'>Separated</Button>
                <Button.Or />
                <Button onClick={this.handleSplit} className={this.state.split === 'none' ? 'yellow' : ''} value='none'>Connected</Button>
              </Button.Group>
              <Button.Group>
                <Button onClick={this.handleStagger} className={this.state.stagger === 'vertical' ? 'yellow' : ''} value='vertical'>Vertical</Button>
                <Button.Or />
                <Button onClick={this.handleStagger} className={this.state.stagger === 'none' ? 'yellow' : ''} value='none'>None</Button>
                <Button.Or />
                <Button onClick={this.handleStagger} className={this.state.stagger === 'horizontal' ? 'yellow' : ''} value='horizontal'>Horizontal</Button>
              </Button.Group>
            </Label.Group>
          </Segment>
        </Segment.Group>
        <Grid stackable columns={2}>
          { renderedKeyboards.map((keyboard) => { return keyboard.keyboard }) }
        </Grid>
      </Page>
    )
  }
}

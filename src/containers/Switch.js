import React from 'react'
import { useRouteData } from 'react-static'
import { Link } from 'components/Router'

export default function Layout() {
  debugger
  const { data } = useRouteData()
  return (
    <div>
      <Link to="/switches/">{'<'} Back</Link>
      <br />
      <h3>{data.manufacturer} - {data.switch}</h3>
      <pre>
        { JSON.stringify(data, null, 2) }
      </pre>
    </div>
  )
}

import React from 'react'
import { useRouteData } from 'react-static'
import { Link } from 'components/Router'

export default function Layout() {
  debugger
  const { data } = useRouteData()
  return (
    <div>
      <Link to="/layouts/">{'<'} Back</Link>
      <br />
      <h3>{data.title}</h3>
      <pre>
        { JSON.stringify(data, null, 2) }
      </pre>
    </div>
  )
}

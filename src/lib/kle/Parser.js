function getEnd(input) {
  let endIndex = input.indexOf('&')
  if(endIndex === -1){ endIndex = input.length }
  if(input.charAt(endIndex - 1) === '/' && input.charAt(endIndex - 2) !== '/') { endIndex = input.indexOf('&', endIndex + 1)}
  return endIndex
}

function rowNeedsNewKey(layout) {
  let row = getRow(layout)
  if(row.length === 0) { return true }
  if(getKey(layout)['legend'] !== undefined) { return true }
  return false
}

function getRow(layout) {
  return layout.rows[layout.rows.length - 1]
}

function getKey(layout) {
  let row = getRow(layout)
  if(!row || row.length < 1){ return undefined }
  return row[row.length - 1]
}

function getPreKey(layout, includePreviousRow=true) {
  let row = getRow(layout)
  let key = getKey(layout)
  if(!row || !key){ return null }
  if(row.length > 1){
    return row[row.length - 2]
  } else if(includePreviousRow && layout.rows.length > 1) {
    let preRow = layout.rows[layout.rows.length - 2]
    return preRow[preRow.length - 1]
  }
}

function updateXOffset(layout) {
  let row = getRow(layout)
  let key = getKey(layout)
  if(!row || !key){ return null }
  key.x = parseFloat(key.x || 0)
  let preKey = getPreKey(layout, false)
  if(!preKey){ return null }
  key.x += parseFloat(preKey.w || 1) + parseFloat(preKey.x || 0)
}

function updateYOffset(layout) {
  let row = getRow(layout)
  let key = getKey(layout)
  if(!row || !key){ return null }
  key.y = parseFloat(key.y || 0)
  let preKey = getPreKey(layout, true)
  if(!preKey){ return null }
  if(row.length > 1){
    key.y += preKey.y
  } else if(parseInt(key.r) === parseInt(preKey.r)) {
    key.y += preKey.y + 1
  }
}

function updateRotation(layout) {
  let row = getRow(layout)
  let key = getKey(layout)
  if(!row || !key){ return null }
  let preKey = getPreKey(layout, true)
  if(!preKey){ return null }
  key.r = parseFloat(key.r || preKey.r || 0)
  key.rx = parseFloat(key.rx || preKey.rx || 0)
  key.ry = parseFloat(key.ry || preKey.ry || 0)
}

function updateDimensions(layout) {
  let key = getKey(layout)
  if(!key){ return null }
  if(key.h) { key.h = parseFloat(key.h || 0) }
  if(key.w) { key.w = parseFloat(key.w || 0) }
  let rowHeight = (key.ry || 0) + Math.sin((key.r || 0) * Math.PI/180) * ((key.x || 0) + (key.w || 1)) + ((key.y || 0) + (key.h || 1))
  let rowWidth = (key.rx || 0) + Math.cos((key.r || 0) * Math.PI/180) * ((key.x || 0) + (key.w || 1))
  if(rowHeight > layout.height){ layout.height = rowHeight }
  if(rowWidth > layout.width){ layout.width = rowWidth }
}

function stripUrlAndHeadersFromInput(input, layout) {
  let updatedInput = unescape(input).replace(/http:\/\/www.keyboard-layout-editor.com\/##@/, '')
  let index = updatedInput.indexOf('@')
  return parse(updatedInput.substring(index, updatedInput.length), layout)
}

function updateImpliedValues(layout) {
  updateRotation(layout)
  updateXOffset(layout)
  updateYOffset(layout)
  updateDimensions(layout)
}

function handleNewRow(input, layout) {
  updateImpliedValues(layout)
  layout.rows.push([])
  return parse(input.substring(1, input.length), layout)
}

function handleNewKey(input, layout) {
  updateImpliedValues(layout)
  getRow(layout).push({})
  return parse(input, layout)
}

function handleLegend(input, layout) {
  if(rowNeedsNewKey(layout)) { return handleNewKey(input, layout) }
  let key = getKey(layout)
  let chunck = input.slice(0, getEnd(input))
  let legend = chunck
    .slice(1, chunck.length)
    .replace('/&', '&')
    .replace('//', '/')
    .replace('/:', ':')
    .replace('/=', '=')
    .replace('/_', '_')
    .replace('/@', '@')
  if(legend.charAt(legend.length - 1) === ';' && legend.charAt(legend.length - 2) !== '/') { legend = legend.slice(0, legend.length - 1) }
  
  key['legend'] = legend.replace('/;', ';').split(/\n|<br>/)
  return parse(input.substring(chunck.length, input.length), layout)
}

function handleSymbol(input, layout) {
  if(rowNeedsNewKey(layout)) { return handleNewKey(input, layout) }
  let chunck = input.slice(0, getEnd(input))
  let symbol = chunck.slice(1, chunck.indexOf(':'))
  getKey(layout)[symbol] = chunck.slice(symbol.length + 2, chunck.length)
  if(chunck.charAt(chunck.length - 1) !== ';') {
    return parse('_' + input.substring(chunck.length + 1, input.length), layout)
  } else {
    getKey(layout)[symbol] = chunck.slice(symbol.length + 2, chunck.length - 1)
    return parse(input.substring(chunck.length + 1, input.length), layout)
  }
}

function handleAmpersand(input, layout) {
  return parse(input.substring(1, input.length), layout)
}


function parse(input, layout={height: 0, width: 0, rows: []}) {
  switch(input.charAt(0)) {
    case 'h':
      return stripUrlAndHeadersFromInput(input, layout)
    case '@':
      return handleNewRow(input, layout)
    case '=':
      return handleLegend(input, layout)
    case '_':
      return handleSymbol(input, layout)
    case '&':
      return handleAmpersand(input, layout)
    default:
      updateImpliedValues(layout)
      return layout
  }
}

export { parse }

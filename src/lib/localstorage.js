export default {
  get: function(key) {
    if(typeof window !== 'undefined'){
      return localStorage.getItem(key)
    }
    return null
  },
  getA: function(key) {
    if(typeof window !== 'undefined'){
      let item = localStorage.getItem(key)
      if(item !== null) {
        return localStorage.getItem(key).split(',').map(function(value) {
          return parseInt(value)
        })
      }
    }
    return null
  },
  set: function(key, value) {
    if(typeof window !== 'undefined'){
      localStorage.setItem(key, value)
    }
  },
  rm: function(key) {
    if(typeof window !== 'undefined'){
      localStorage.removeItem(key)
    }
  }
}

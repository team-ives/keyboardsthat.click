import Keyboard from '../components/layouts/Keyboard'
import ls from './localstorage'
import { knuthShuffle } from 'knuth-shuffle'
import importAll from './importAll'
const keyboards = importAll(require.context('../../layouts/', true, /\.json$/))

export default class Data {
  constructor() {
    this.keyboards = keyboards.map((data) => {
      let layout = data.kle
      let layouts = [data.kle]
      if(Array.isArray(data.kle)) {
        let index = Math.floor(Math.random() * data.kle.length)
        layout = data.kle[index]
        layouts = data.kle.splice(index)
      }
      data.key = layout
      data.keyboard = new Keyboard({id: data.id, kle: layout, layouts: layouts, description: data.description, title: data.title})
      return data
    })
  }

  initialState() {
    let maxWidth = 0
    let minWidth = this.keyboards[0].width
    let maxHeight = 0
    let minHeight = this.keyboards[0].height

    this.keyboards.forEach((keyboard) => {
      if(keyboard.width > maxWidth) maxWidth = keyboard.width
      if(keyboard.width < minWidth) minWidth = keyboard.width
      if(keyboard.height > maxHeight) maxHeight = keyboard.height
      if(keyboard.height < minHeight) minHeight = keyboard.height
    })

    return { 
      currentWidth: ls.getA('currentWidth') || [minWidth, maxWidth],
      maxWidth: maxWidth,
      minWidth: minWidth,
      maxHeight: maxHeight,
      minHeight: minHeight,
      currentHeight: ls.getA('currentHeight') || [minHeight, maxHeight],
      split: ls.get('currentSplit') || undefined,
      stagger: ls.get('currentStagger') || undefined,
    }
  }

  filteredKeyboards(state) {
    return knuthShuffle(this.keyboards.reduce((boards, keyboard) => {
      if(keyboard.width >= state.currentWidth[0] &&
         keyboard.height >= state.currentHeight[0] &&
         keyboard.width <= state.currentWidth[1] &&
         keyboard.height <= state.currentHeight[1] &&
         (state.stagger === undefined || keyboard.stagger === state.stagger) &&
         (state.split === undefined || keyboard.split === state.split)) boards.push(keyboard)
      return boards
    }, []))
  }
}

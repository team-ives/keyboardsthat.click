export default function importAll(files) {
  let imported = []

  files.keys().forEach(key => {
    let data = files(key)
    data.id = key.replace('.json', '')
    imported.push(data)
  })

  return imported
}
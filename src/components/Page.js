import React from 'react';
import { Link } from 'components/Router'
import Logo from '../svg/logo.svg'
import { Header, Segment } from 'semantic-ui-react'

let Page = (props) => {
  return(
    <div className='keyboard-app'>
      <Header as='h2' className='app-header' icon textAlign='center'>
        <Header.Content>
          <Logo className="logo"></Logo>
        </Header.Content>
      </Header>
      <Segment.Group>
        <nav>
          <Link to="/">Home</Link>
          <Link to="/layouts">Layout Compendium</Link>
          <Link to="/vendors">Keyboard Vendors</Link>
        </nav>
      </Segment.Group>
      <div className="content">
        { props.children }
      </div>
    </div>
  )
}

export default Page;

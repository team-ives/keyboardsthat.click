import React from 'react';

let Key = (props) => {
  return (
    <a href={props.url} >
      <h3>
        { props.name }
      </h3>
    </a>
  );
}

export default Key;

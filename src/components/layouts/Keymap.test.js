import React from 'react'
import { shallow } from 'enzyme'
import Keymap from './Keymap'
import Key from  './Key'

it('renders a 1x1 key with only a legend', () => {
  let kleLink = 'http://www.keyboard-layout-editor.com/##@@_a:7%3B&=Tab&=Q&=W&=E&=R&=T&=Y&=U&=I&=O&=P&=Back%20Space%3B&@=Esc&=A&=S&=D&=F&=G&=H&=J&=K&=L&=%2F%3B&=\'%3B&@=Shift&=Z&=X&=C&=V&=B&=N&=M&=,&=.&=%2F%2F&=Return%3B&@=&=Ctrl&=Alt&=Super&=%2F&dArr%2F%3B&_w:2%3B&=&=%2F&uArr%2F%3B&=%2F&larr%2F%3B&=%2F&darr%2F%3B&=%2F&uarr%2F%3B&=%2F&rarr%2F'
  const subject = shallow(<Keymap link={ kleLink } />)
  expect(subject.find(Key).length).toEqual(47)
})

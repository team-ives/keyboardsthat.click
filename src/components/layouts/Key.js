import React from 'react';

let Key = (props) => {
  const legends = props.legend.map((text, index) => {
    return(<text key={ text+index } x='5' y={15 + index * 25} fontSize='.9em' fill='white'>{text}</text>)
  })
  let transform = `translate(${50 * (props.rx || 0)} ${50 * (props.ry || 0)}) rotate(${props.r || 0}) translate(${50 * (props.x || 0)} ${50 * (props.y || 0)})` 
  return (
    <g transform={transform}>
      <rect width={50 * (props.w || 1)} height={50 * (props.h || 1)} style={{stroke: 'white'}}>
      </rect>
      {legends}
    </g>
  );
}

export default Key;

import React from 'react';
import { Segment, Popup, Button } from 'semantic-ui-react'

let Keyboard = (props) => {
  let route = null
  let kleLink = null
  if(props.layoutsLink){
    route = <Popup trigger={<Button as='a' href={`/keyboards/${props.layoutsLink}`} content={`View Other ${props.title} Layouts`} icon='keyboard' labelPosition='right' />} position='right center'>
              View other key configurations for this form factor
            </Popup>
  }
  if(props.kleLink){
    kleLink = <Popup trigger={<Button as='a' href={props.kleLink} content='Edit in Keyboard Layout Editor' icon='edit' labelPosition='left' />} position='left center'>
              Edit this layout in Keyboard Layout Editor
            </Popup>
  }
  return (
    <Segment textAlign='center' attached>
      {kleLink}
      {route}
    </Segment>
  );
}

export default Keyboard;

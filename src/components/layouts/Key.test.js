import React from 'react'
import { shallow } from 'enzyme'
import Key from './Key'

function subject(params) {
  return shallow(<Key { ...params } />)
}

it('renders a 1x1 key with only a legend', () => {
  let params = {legend: []}
  expect(subject(params).html()).toEqual('<g transform="translate(0 0) rotate(0) translate(0 0)"><rect width="50" height="50" style="stroke:white"></rect></g>')
})

it('renders a 1.25x1 key a single legend', () => {
  let params = {legend: ['foo'], w: 1.25}
  expect(subject(params).html()).toEqual('<g transform="translate(0 0) rotate(0) translate(0 0)"><rect width="62.5" height="50" style="stroke:white"></rect><text x="5" y="15" font-size=".9em" fill="white">foo</text></g>')
})

it('renders a 1.5x1 key two legends', () => {
  let params = {legend: ['foo', 'bar'], w: 1.25}
  expect(subject(params).html()).toEqual('<g transform="translate(0 0) rotate(0) translate(0 0)"><rect width="62.5" height="50" style="stroke:white"></rect><text x="5" y="15" font-size=".9em" fill="white">foo</text><text x="5" y="40" font-size=".9em" fill="white">bar</text></g>')
})

it('renders a key at an angle', () => {
  let params = {legend: [], r: 1, rx: 2, ry: 3, x: 4, y: 5, w: 6, h: 7}
  expect(subject(params).html()).toEqual('<g transform="translate(100 150) rotate(1) translate(200 250)"><rect width="300" height="350" style="stroke:white"></rect></g>')
})

test.todo('renders an iso enter key with two heights')

import React from 'react'
import { shallow } from 'enzyme'
import { Popup } from 'semantic-ui-react'
import LinkSegment from './LinkSegment'

it('generates 2 popups if both route and kle are included', () => {
  let kleLink = 'http://something.something'
  const subject = shallow(<LinkSegment kleLink={ kleLink } layoutsLink='abcd'/>)
  expect(subject.find(Popup).length).toEqual(2)
})

it('generates 1 popup if kle link is included', () => {
  let kleLink = 'http://something.something'
  const subject = shallow(<LinkSegment kleLink={ kleLink } />)
  expect(subject.find(Popup).length).toEqual(1)
})

it('generates 1 popup if route is included', () => {
  const subject = shallow(<LinkSegment layoutsLink='abcd' />)
  expect(subject.find(Popup).length).toEqual(1)
})

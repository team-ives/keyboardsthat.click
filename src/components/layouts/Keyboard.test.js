import React from 'react'
import { shallow } from 'enzyme'
import Keyboard from './Keyboard'
import Keymap from './Keymap'

const keyboard = {
  layout: 'keyboard',
  title: 'Standard',
  date: '2018-01-07T16:33:42-0400',
  height: 6,
  width: 22,
  split: false,
  stagger: 'none',
  image_link: 'something/something.png',
  description: 'The 9key is a macropad with 9 keys.',
  layouts: [
    {
      kle: 'http://www.keyboard-layout-editor.com/##@@=3&=2&=1%3B&@=4&=5%0AEnter&=6%3B&@=7%0A0&=8&=9%0AFN',
      description: 'Reversed top row.'
    },
    {
      kle: 'http://www.keyboard-layout-editor.com/##@@=3&=2&=1%3B&@=4&=5%0AEnter&=6%3B&@=8%0A0&=8&=7%0AFN',
      description: 'Reversed top and bottom row.'
    }
  ]
}

it('renders the default layout linked to keyboard layout editor', () => {
  const wrapper = shallow(<Keyboard { ...keyboard } />)
  expect(wrapper.find(Keymap).length).toBe(1)
})

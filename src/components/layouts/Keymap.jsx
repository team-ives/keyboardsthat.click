import React from 'react';
import Key from './Key'
import { parse } from '../../lib/kle/Parser'

let Keymap = (props) => {
  let layout = parse(props.link)

  let keyMap = layout.rows.map((row) => {
    return(row.map((key, index) => {
      return(<Key {...key} key={index+key.toString()} />)
    }))
  });
  return (
    <div>
      <svg viewBox={`0 0 ${layout.width*50} ${layout.height*50}`} xmlns="http://www.w3.org/2000/svg">
        {keyMap}
      </svg>
    </div>
  );
}

export default Keymap;

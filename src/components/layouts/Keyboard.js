import React from 'react';
import Keymap from './Keymap'
import { Grid, Card } from 'semantic-ui-react'
import { Link } from '../../components/Router'

let Keyboard = (props) => {
  return (
    <Grid.Column key={props.key} >
      <Card fluid raised>
        <Card.Content>
          <Card.Header textAlign='center' as='h2'>
          <Link to={ `layouts/${props.id}` }>{ props.title }</Link>
          </Card.Header>
        </Card.Content>
        <Card.Content textAlign='center' >
          <a href={ props.kle } className='layout'><Keymap link={ props.kle }/></a>
        </Card.Content>
        <Card.Content extra textAlign='center'>
          { props.description }
        </Card.Content>
      </Card>
    </Grid.Column>
  );
}

export default Keyboard;

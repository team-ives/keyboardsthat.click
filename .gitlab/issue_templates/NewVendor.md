# Please set the title to "Add Vendor: <Vendor Name>"

Fill out the following:
```
"name": "name-of-vendor",
"url": "url of store",
"description": "describe what kind of stuff you sell"
```

/label ~new-vendor
